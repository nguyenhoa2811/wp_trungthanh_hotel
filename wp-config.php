<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_thanhtrung_hotel');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_CmZ{~%Gfr+kL}{Vh(h%r/~Ia@+.1N&sM+qGSSrx/^y-xMPI)3=PMDDt+4y.^s7A');
define('SECURE_AUTH_KEY',  '|iKWkdeX>::M+#C;4wa7AZ2,|W+=r_|:7_`V$!}b(IDQ*=8*:-gG@/ZM-$$fxo)w');
define('LOGGED_IN_KEY',    'BSmk[Ah2F:-w.37@`ffZ.UIa`|joIHHA4S#}~/AnUdvqdg3i2UL5cno+r0aya.ex');
define('NONCE_KEY',        '05! $_qKvNdsZopdJW?P(COe`*X`Vo7oM+Ir3V,,!qh8D^kidZKFU=0c.y^1&kqT');
define('AUTH_SALT',        '2DCF!<`CDcy{Xv8vkrE1k;tr/#yvjOkKo)p@omiw;jc3<r*TEH]F2:<zbPsw3bwz');
define('SECURE_AUTH_SALT', 'hEm^.|!uJ91A!n2;~/a=H+*QOTJCaC0mzq.zrZOG|jmwMW_G^QvT:(UyzW9+p}!*');
define('LOGGED_IN_SALT',   'Zf:,0a>142i[Km9y4F+1qB+_PHD<(rM&MpikL3Ay^-#=K{,I[SOq3ES5?~3Py<hR');
define('NONCE_SALT',       'XeUH<Zxd^k3dcRT2.<q#tuC8)dP9TqY^P1tOr7XiuP|;A7d}u+iDAIR-+#F/Q,:8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_ttht_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
